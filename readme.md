# Tarnów pod ręką
Simple application with city map, events, walking routes, and report problems in city created on Hackhaton '**<Zaprogramuj_Tarnów/>** in 2019' in **36 hours**.

## Table of Contents
 - [General info](#general-info) 
 - [Technologies](#technologies) 
 - [Setup](#setup) 
 
## General info 
The application allows to **check the location of important places** and **events in Tarnów**. We can also **check walking routes** with the most interesting points marked on the map. Additional we can **play Quiz** and **report problem** in city.

| *Main Screen*  | *Menu*  | *Map view*  |
| -- | -- | -- |
| ![Register](https://i.ibb.co/3yyK0DY/tpd9.png "Main Screen") | ![Register](https://i.ibb.co/FnXJ3CD/tpd8.png "Menu") | ![Register](https://i.ibb.co/VTXfg0p/tpd7.png "Map Screen") |
| *Events screen* | *Walking route screen* | *Add route screen* |
|  ![Register](https://i.ibb.co/JKFp0hY/tpd4.png "Events Screen") | ![Register](https://i.ibb.co/4YwY9FM/tpd6.png "Walking Route Screen") | ![Register](https://i.ibb.co/Y75LF94/tpd5.png "Add walking route Screen") |
| *Report problem screen* | *Weather screen* | *QUIZ screen* |
|  ![Register](https://i.ibb.co/RD2zpH3/tpd3.png "Report Problem Screen") | ![Register](https://i.ibb.co/dr2GMxt/tpd2.png "Weather Screen") | ![Register](https://i.ibb.co/x7vzrN3/tpd.png "QUIZ Screen") |

## Technologies
 - Node.js 12.2.0 (server API)
 - React 16.5.0 
 - Redux 4.0.1
 
## Setup 
To run server: install ``` npm install``` and run ``` npm start ``` 
  
To run frontend: install``` npm install ``` and run``` expo start ``` 
