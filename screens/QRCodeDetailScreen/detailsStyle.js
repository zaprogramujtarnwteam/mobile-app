import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const windowHeight = window.height;
export const windowWidth = window.width;

export default StyleSheet.create({
    placesContainer: {
        flex: 1,
        flexDirection: 'column',
    },
    company: {
        margin: 5,
        textAlign: "center",
        fontWeight: "300",
        fontSize: 25
    },
    organizer: {
        margin: 5,
        textAlign: "center",
        fontWeight: "100",
        fontSize: 20,
        color: "grey"
    },

    description: {
        marginTop: 10,
        marginBottom: 10,
        textAlign: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#d6d5df",
        paddingBottom: 10,
    },

    
    contactInfo: {
        textAlign: "center",
        marginTop: 5,
        fontSize: 15
    },

    contactInfoLast: {
        textAlign: "center",
        marginTop: 5,
        fontSize: 15,
        borderBottomWidth: 1,
        borderBottomColor: "#d6d5df",
        color: "#285398",
        paddingBottom: 10,
    },
});
