import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, ImageBackground, Linking, TouchableOpacity } from 'react-native';
import Config from '../../Config';

// ours components
import Header from '../../components/Header/Header';
import styles from './detailsStyle';

export default class QRCodeDetailScreen extends Component {
    static navigationOptions = {
        drawerLabel: () => null
    }
    state = {
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: "column" }}>
                <StatusBar hidden />
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                <View style={{ margin: 15, marginTop: 35, flex: 1 }}>
                    {this.props.navigation.state.params.type === "card"?
                        <View style={styles.placesContainer}>
                        <View style={{flex: 5}}>
                            
                        <Text style={styles.organizer}>Numer karty: {this.props.navigation.state.params.data}</Text>
                            <Text style={styles.description}>Masz jeszcze 20.12 zł na karcie</Text>
                            <Text style={styles.description}>Twoja karta jest ważna do 10.04.2020</Text>
                        </View>
                    </View>
                    :
                    //{ uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.navigation.state.params.data.imagePath}` } : require('../../assets/images/homescreen/eventsscreen.png')} style={{ width: "100%", flex: 1, resizeMode: "cover" }
                        <View style={styles.placesContainer}>
                            <ImageBackground source={{ uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.navigation.state.params.data.imagePath}` }} style={{ width: "100%", flex: 2, resizeMode: "cover" }
                                } imageStyle={{ borderRadius: 5 }}>
                            </ImageBackground >
                            <View style={{flex: 5}}>
                                <Text style={styles.company}>{this.props.navigation.state.params.data.name}</Text>
                            <Text style={styles.organizer}>{this.props.navigation.state.params.data.category} / {this.props.navigation.state.params.data.subcategory} {Array(this.props.navigation.state.params.data.priceLevel).fill("$").join("")}</Text>
                                <Text style={styles.description}>{this.props.navigation.state.params.data.description}</Text>
                                
                                {this.props.navigation.state.params.data.workingHours[0]? <Text style={styles.contactInfo}>Godziny otwarcia: {this.props.navigation.state.params.data.workingHours[0]} - {this.props.navigation.state.params.data.workingHours[1]}</Text> : null}
                                {this.props.navigation.state.params.data.phone? <Text style={styles.contactInfo}>Telefon: {this.props.navigation.state.params.data.phone}</Text> : null}
                                {this.props.navigation.state.params.data.email? <Text style={styles.contactInfo}>Email: {this.props.navigation.state.params.data.email}</Text> : null}
                                <TouchableOpacity onPress={() => Linking.openURL(this.props.navigation.state.params.data.website)}>
                                    <Text style={styles.contactInfoLast}>
                                        {this.props.navigation.state.params.data.website}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </View>
            </View>
            //workingHours
            //website
            //email phone

            //longitude,latitude
            //address

        );
    }
}
