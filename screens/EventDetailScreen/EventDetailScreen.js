// modules
import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, ImageBackground, TouchableOpacity, Linking } from 'react-native';
// ours components
import Header from '../../components/Header/Header';
import Config from '../../Config';
import styles from './detailStyles'

export default class EventDetailScreen extends Component {
    static navigationOptions = {
        drawerLabel: () => null
    }
    state = {
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: "column" }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                <View style={{ margin: 15, marginTop: 35, flex: 1, flexDirection: "column" }}>
                    < ImageBackground source={this.props.navigation.state.params.data.imagePath ? { uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.navigation.state.params.data.imagePath}` } : require('../../assets/images/homescreen/eventsscreen.png')} style={{ width: "100%", flex: 1, resizeMode: "cover" }
                        } imageStyle={{ borderRadius: 5 }}>
                    </ImageBackground >
                    <View style={{flex: 2}}>
                        <Text style={styles.company}>{this.props.navigation.state.params.data.name}</Text>
                        <Text style={styles.organizer}>Organizator: {this.props.navigation.state.params.data.organizer}</Text>
                        <Text style={styles.description}>{this.props.navigation.state.params.data.description}</Text>
                        {/* <Text>{JSON.stringify(this.props.navigation.state.params.data)}</Text> */}
                        <Text style={styles.contactInfo}>Czas wydarzenia: {new Date(this.props.navigation.state.params.data.startDate).toLocaleDateString() + " - " + new Date(this.props.navigation.state.params.data.endDate).toLocaleDateString()}</Text>
                        <Text style={styles.contactInfo}>Adres: {this.props.navigation.state.params.data.address}</Text>
                        <TouchableOpacity onPress={() => Linking.openURL(this.props.navigation.state.params.data.website)}>
                            <Text style={styles.contactInfoLast}>
                                {this.props.navigation.state.params.data.website}
                            </Text>
                        </TouchableOpacity>
                        {console.log(this.props.navigation.state.params.data.price , this.props.navigation.state.params.data.price !== 0)}
                        {(this.props.navigation.state.params.data.price)?<Text style={styles.contactInfo}>Cena biletu: {this.props.navigation.state.params.data.price}</Text>: null}
                        <Text style={styles.contactInfo}>Kategoria: {this.props.navigation.state.params.data.category}</Text>
                    </View>

                </View>
            </View>
        );
    }
}
