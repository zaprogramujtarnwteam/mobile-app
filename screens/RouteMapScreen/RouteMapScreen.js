import React, { Component } from 'react';
import { View, Text, StatusBar, Image, TouchableHighlight, Dimensions, StyleSheet, Modal, ScrollView } from 'react-native';
import * as Expo from 'expo';
import darkMapStyle from '../../style/darkMapStyle'
import lightMapStyle from '../../style/lightMapStyle'
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';

import Header from '../../components/Header/Header'
import MapViewDirections from 'react-native-maps-directions';
import Config from '../../Config';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0322;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = 'AIzaSyARoem-eQySUngW07bG8TudyMf2aU6rBWA';
let points = [];
//AIzaSyBDUOqV8qDD58vkzjUaJCc9tbWIbqYWoXQ


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        backgroundColor: '#ecf0f1',
    },
});

class RouteMapScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Mapa Trasa',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf279; </Text>
        ),
    };

    state = {
        coordinates: [
            {
                latitude: 37.3317876,
                longitude: -122.0054812,
            },
            {
                latitude: 37.771707,
                longitude: -122.4053769,
            },
        ],
        region: {
            latitude: 50.01381,
            longitude: 20.98698,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        modalVisible: false,
        title: '',
        description: '',
        uri: '',
    };

    onPress = () => {
        if (this.state.mapStyle !== darkMapStyle)
            this.setState({ mapStyle: darkMapStyle, switchOn: !this.state.switchOn });
        else
            this.setState({ mapStyle: lightMapStyle, switchOn: !this.state.switchOn });

    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    static getDerivedStateFromProps(props, state) {
        // do things with nextProps.someProp and prevState.cachedSomeProp
        const data = props.navigation.state.params;
        return { ...data };
    }
    // ******************************
    // miejsca są w this.props.places
    // ******************************

    render() {
        if (this.state.item.route.length > 5  ) {
            const waypoints = this.state.item.route.slice(1, -1);
            const n = Math.floor(waypoints / 23);
            points = [];
            for (var i = 0; i < 23; i += 3) {
                points.push(waypoints[i]);
            }
        }

        return (
            <View style={{ flex: 1, flexDirection: "column", position: 'relative', zIndex: 9 }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                <StatusBar hidden />
                <MapView
                    initialRegion={{
                        latitude: this.state.item.route[0].latitude,
                        longitude: this.state.item.route[0].longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }}
                    style={StyleSheet.absoluteFill}
                    ref={c => this.mapView = c}
                >
                    {
                        this.state.item.crucialPoints ?
                            this.state.item.crucialPoints.map((element) => {
                                return (
                                    <Marker
                                        key={element._id}
                                        coordinate={element}
                                        title={element.name}
                                        onPress={() => { this.setState({ title: element.name, description: element.description, uri: element.imagePath, modalVisible: true }); }}
                                    // description={
                                    //     element.description
                                    // }  
                                    />
                                )
                            })
                            : null
                    }
                    <MapViewDirections
                        origin={this.state.item.route[0]}
                        waypoints={(this.state.item.route.length > 2) ? points : null}
                        destination={this.state.item.route[this.state.item.route.length - 1]}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={3}
                        strokeColor="#285398"
                        mode='walking'
                    />
                </MapView>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <ScrollView>
                        <View style={{ margin: 15, marginTop: 30 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 18, marginBottom: 10 }}>{this.state.title}</Text>
                            <Text style={{ fontFamily: 'lato', fontSize: 16 }}>{this.state.description}</Text>
                            <Image source={{ uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.state.uri}` }} style={{ width: width - 50, height: width - 50 }}></Image>
                        </View>
                        <TouchableHighlight
                            onPress={() => {
                                this.setModalVisible(!this.state.modalVisible);
                            }}
                            style={{ position: 'absolute', top: 5, right: 20, zIndex: 9999 }}
                        >
                            <Text style={{ fontFamily: 'fontAwesome', fontSize: 40, }}>&#xf00d;</Text>
                        </TouchableHighlight>
                    </ScrollView>
                </Modal>
            </View >
        );
    }
}

const mapStateToProps = state => {
    return {
        places: state.data.Place
    };
};

export default connect(mapStateToProps)(RouteMapScreen);