import React, { Component } from 'react';
import { View, KeyboardAvoidingView, StatusBar, TouchableOpacity, Image, Text, Dimensions, ScrollView, ToastAndroid, Picker } from 'react-native';
import { ImagePicker } from 'expo';
const { width } = Dimensions.get('window');
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import lightMapStyle from '../../style/lightMapStyle';
import MyInput from '../../components/MyInput';
import Config from '../../Config.json';
import axios from 'axios';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

class AddCheckPoint extends Component {
    static navigationOptions = {
        drawerLabel: () => null,
    };

    state = {
        name: null,
        description: null,
        startDate: null,
        endDate: null,
        website: null,
        ticketWebsite: null,
        price: null,
        organizer: null,
        category: "Informatyczne",
        image: null,
        imageName: null,
        address: null
    }

    onChangeText = (key, value) => this.setState({ [key]: value });

    send = async () => {
        const { name, description, startDate, endDate, location, price, organizer, category, ticketWebsite, website, address } = this.state;

        if (!name || !description || !startDate || !endDate || !organizer || !category || !address) {
            console.log(!name,!description,!startDate,!endDate,!price,!organizer,!category,!address)
            ToastAndroid.showWithGravity(
                'Proszę uzupełnić dane w formularzu',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
        else {
            try {
                const res = await axios.post(`${Config.serverIP}:${Config.serverPORT}/api/event/add`, {
                    name, description, startDate, endDate, price, organizer, category, ticketWebsite, website, address,
                    imagePath: this.state.imageName ? this.state.imageName : null
                });

                //console.log(res.data);

                const events = await axios.get(`${Config.serverIP}:${Config.serverPORT}/api/event/all`);

                ToastAndroid.showWithGravity(
                    'Pomyślnie dodano wydarzenie',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );

                this.props.fetchEvents(events.data.events);
                //redirect to Events screen
                setTimeout(() => this.props.navigation.navigate("Wydarzenia"), 1000);

            } catch (err) {
                console.log(err);
            }
        }
    }

    render() {
        let { image } = this.state;

        return (
            <KeyboardAvoidingView style={{ margin: 10, flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <StatusBar hidden />

                    <MyInput label={'Nazwa wydarzenia'} action={'input'} change={'name'} onChangeText={this.onChangeText.bind(null, "name")} />
                    <MyInput label={'Opis wydarzenia'} action={'input'} change={'description'} onChangeText={this.onChangeText.bind(null, "description")} />
                    <MyInput label={'Strona wydarzenia'} action={'input'} change={'website'} onChangeText={this.onChangeText.bind(null, "website")} />
                    <MyInput label={'Strona kupna biletów'} action={'input'} change={'ticketWebsite'} onChangeText={this.onChangeText.bind(null, "ticketWebsite")} />
                    <MyInput label={'Cena'} action={'input'} change={'price'} onChangeText={this.onChangeText.bind(null, "price")} />
                    <MyInput label={'Organizator'} action={'input'} change={'organizer'} onChangeText={this.onChangeText.bind(null, "organizer")} />
                    <MyInput label={'Adres wydarzenia'} action={'input'} change={'address'} onChangeText={this.onChangeText.bind(null, "address")} />
                    <View style={{borderWidth: 0.5, borderColor: 'rgba(0, 0, 0, 0.5)', borderRadius: 5, padding: 5, marginTop: 5, marginBottom: 5}}>
                        <Text>Kategoria:</Text>
                        <Picker
                            selectedValue={"Informatyczne"}
                            style={{height: 75, width: width-30}}
                            onValueChange={(itemValue, itemIndex) => {
                                console.log(itemValue)
                                this.setState({category: itemValue})
                            }
                            }>
                            <Picker.Item label="Kulturowe" value="Kulturowe" />
                            <Picker.Item label="Informatyczne" value="Informatyczne" />
                            <Picker.Item label="Sportowe" value="Sportowe" />
                            <Picker.Item label="Miejskie" value="Miejskie" />
                            <Picker.Item label="Ekonomiczne" value="Ekonomiczne" />
                            <Picker.Item label="Historyczne" value="Historyczne" />
                            <Picker.Item label="Inne" value="Inne" />
                        </Picker>
                    </View>
                    <Text style={{ width: (width - 20), fontFamily: 'lato', fontSize: 16 }}>Data rozpoczęcia</Text>
                    <MyInput label={'Data rozpoczęcia'} action={'date'} change={'dateStart'} onChangeText={this.onChangeText.bind(null, "startDate")} />

                    <Text style={{ width: (width - 20), fontFamily: 'lato', fontSize: 16 }}>Data zakończenia</Text>
                    <MyInput label={'Data zakończenia'} action={'date'} change={'dateEnd'} onChangeText={this.onChangeText.bind(null, "endDate")} />

                    <View style={{ flex: 1, alignItems: 'center', marginTop: 10, justifyContent: 'flex-start' }}>
                        <TouchableOpacity onPress={() => this._pickImage()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Dodaj zdjęcie</Text>
                        </TouchableOpacity>
                        {image &&
                            <Image source={{ uri: image }} style={{ width: (width - 100), height: (width - 100) }} />}
                        <TouchableOpacity onPress={() => this.send()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Zatwierdź</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false, // higher res on iOS
            aspect: [4, 3],
        });

        if (result.cancelled) {
            return;
        }

        let localUri = result.uri;
        let filename = localUri.split('/').pop();

        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        let formData = new FormData();
        formData.append('image', { uri: localUri, name: filename, type });

        const res = await fetch(`${Config.serverIP}:${Config.serverPORT}/api/image/add`, {
            method: 'POST',
            body: formData,
            header: {
                'content-type': 'multipart/form-data',
            },
        });

        const data = await res.json();

        this.setState({ image: result.uri, imageName: data.fileName });
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchEvents: (events) => dispatch(actionCreators.fetchEvents(events))
    }
}

export default connect(null, mapDispatchToProps)(AddCheckPoint);