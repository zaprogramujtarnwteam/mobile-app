import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../HomeScreen/categoriesStyle';
import Question from '../../components/Question';

import axios from 'axios';

import Config from '../../Config';

import Header from '../../components/Header/Header';

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export default class QuizScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Rozwiąż quiz',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf059;</Text>
        ),
    };
    state = {
        loading: true,
        questions: [],
        answers: [0, 0, 0, 0, 0],
        checkPressed: false
    }
    componentDidMount = async () => {
        try {
            const res = await axios.get(`${Config.serverIP}:${Config.serverPORT}/api/question`);
            for (let i = 0; i < res.data.questions.length; i++) {
                res.data.questions[i].answers = shuffle(res.data.questions[i].answers)
            }
            this.setState({ loading: false });
            this.setState({ questions: res.data.questions });
        } catch (error) {
            console.log(error);
        }
    }

    verify = (n, ans) => {
        let a = this.state.answers.slice();
        a[n] = ans ? 1 : 0;
        this.setState({ answers: a });
    }
    checkQuiz = () => {
        this.setState({ checkPressed: true });
    }
    render() {
        return (
            <View style={{ flex: 1, flexDirection: "column" }}>
                {this.state.loading ? <Spinner
                    visible={this.state.loading}
                    textContent={'Wczytywanie pytań...'}
                    textStyle={styles.spinnerTextStyle}
                    overlayColor={'rgba(0, 0, 0, 0.4)'}
                    animation="fade"
                /> : null}

                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                {this.state.questions.length <= 0 || this.state.checkPressed ? null :
                    <>
                        <View style={{ margin: 15, flex: 1, marginTop: 30 }}>
                            <FlatList
                                data={this.state.questions}
                                keyExtractor={x => x.question}
                                renderItem={(item) => <Question data={item} verify={this.verify} />}
                            />
                        </View>
                        <TouchableOpacity onPress={this.checkQuiz} style={{ alignSelf: 'center', backgroundColor: "#285398", padding: 5, borderRadius: 5, margin: 10 }}><Text style={{ color: "white" }}>Sprawdź odpowiedzi</Text></TouchableOpacity>
                    </>
                }
                {
                    this.state.checkPressed ?
                        <View style={{ margin: 15, flex: 1, marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                            <Text>Twój wynik to {this.state.answers.reduce((a, b) => a + b, 0)}/5</Text>
                            {this.state.answers.reduce((a, b) => a + b, 0) == 5 ? <View style={{ margin: 15, flex: 1, marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ textAlign: "center" }}>Gratulacje! W nagrodę za bezbłędnie rozwiązany quiz otrzymujesz kod na 20% żniżki do dowolnego zamówienia w restauracji U Zbycha</Text>
                                <View>
                                    <Text style={{ fontSize: 40, marginTop: 20 }}>{Math.random().toString(36).substr(2, 5)}</Text>
                                </View>
                            </View> : null}
                        </View>
                        : null}
            </View>
        );
    }
}
