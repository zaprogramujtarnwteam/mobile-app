import React, { Component } from 'react';
import { View, Text, StatusBar, WebView, Image, TouchableOpacity } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, MyCustomMarkerView } from 'react-native-maps';
import darkMapStyle from '../../style/darkMapStyle'
import lightMapStyle from '../../style/lightMapStyle'
import SwitchToggle from 'react-native-switch-toggle';
const restaurant = require('../../assets/images/restaurant.png');

import { connect } from 'react-redux';

import Header from '../../components/Header/Header'

const icons = {
    "Sztuka i rozrywka": '&#xf630;',
    "Rekreacja": '&#xf70c;',
    "Muzea i zabytki": "&#xf66f;",
    "Gastronomia": "&#xf2e7;",
    "Handel i usługi": "&#xf291;"
}

class MapScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Mapa',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf279; </Text>
        ),
    };

    state = {
        region: {
            latitude: 50.01381,
            longitude: 20.98698,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        marker: {
            latitude: 50.01381,
            longitude: 20.98698,
        },
        mapStyle: darkMapStyle,
        renderStops: false,
        renderBikesStation: true,

    }

    onPress = () => {
        if (this.state.mapStyle !== darkMapStyle)
            this.setState({ mapStyle: darkMapStyle, switchOn: !this.state.switchOn });
        else
            this.setState({ mapStyle: lightMapStyle, switchOn: !this.state.switchOn });

    }

    // ******************************
    // miejsca są w this.props.places
    // ******************************

    render() {
        return (
            <View key={this.state.mapStyle} style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: "column", position: 'relative', zIndex: 9 }}>
                    <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                    <StatusBar hidden />
                    <MapView
                        region={this.state.region}
                        provider={PROVIDER_GOOGLE}
                        style={{ flex: 1, position: 'relative', zIndex: 0, }}
                        customMapStyle={this.state.mapStyle}
                    >
                    {/* RENDER BUS STOPS */}
                    {this.state.renderStops?
                        this.props.stops.map((item,index) => {
                            return <Marker
                                    key={index}
                                    coordinate={{latitude: item.latitude, longitude: item.longitude}}
                                    title={item.fullStopName}
                                    description={item.street}
                                    anchor={{x: 0.20, y: 0.5}}
                                    pinColor={"#285398"}
                                    >
                                    </Marker>
                        })
                    :
                        null
                    }
                    {/* RENDER BIKES STATIONS */}
                    {this.state.renderBikesStation?
                        this.props.bikeStations.map((item,index) => {
                            return <Marker
                                    key={index}
                                    coordinate={{latitude: item.latitude, longitude: item.longitude}}
                                    title={"Stacja "+item.name}
                                    description={item.availableBikes.toString()+"/"+item.standsAmount.toString()}
                                    anchor={{x: 0.20, y: 0.5}}
                                    >
                                        <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "#F7D100"  }}>&#xf206; </Text>
                                    </Marker>
                        })
                    :
                        null
                    }

                    {/* RENDER PLACES */}
                    {this.props.places?
                        this.props.places.map((item,index) => {
                            return <Marker
                                        key={index}
                                        //workingHours,category,subcategory
                                        coordinate={{latitude: item.latitude, longitude: item.longitude}}
                                        title={item.name}
                                        description={item.workingHours[0]+" - "+item.workingHours[1]+"   "+item.description}
                                        anchor={{x: 0.20, y: 0.5}}
                                    >   
                                {
                                    item.category === "Sztuka i rozrywka" ? <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "#26A65B"  }}>&#xf630;</Text> : 
                                    item.category === "Rekreacja" ? <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "#285398"  }}>&#xf70c;</Text> : 
                                    item.category === "Muzea i zabytki" ? <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "#CA6924"  }}>&#xf66f;</Text> : 
                                    item.category === "Gastronomia" ? <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "darkred"  }}>&#xf2e7;</Text> : 
                                    <Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: "#BF55EC"  }}>&#xf291;</Text>
                                }
                                        
                                    </Marker>
                        })
                    :
                        null
                    }
                    
                    </MapView>
                    <View style={{
                        position: 'absolute',
                        top: 70,
                        right: 5,
                        zIndex: 999999,
                    }}>
                        <SwitchToggle
                            containerStyle={{
                                width: 98,
                                height: 38,
                                borderRadius: 25,
                                backgroundColor: '#ccc',
                                padding: 5,
                            }}
                            backgroundColorOn='#e5e1e0'
                            backgroundColorOff='#e5e1e0'
                            circleStyle={{
                                width: 38,
                                height: 38,
                                borderRadius: 19,
                                backgroundColor: 'white', // rgb(102,134,205)
                            }}
                            switchOn={this.state.switchOn}
                            onPress={this.onPress}
                            circleColorOff='#2b2b2b'
                            circleColorOn='#aadaff'
                            duration={300}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        places: state.data.Place,
        stops: state.data.BusStop,
        bikeStations: state.data.BikeStation,
    };
};

export default connect(mapStateToProps)(MapScreen);