import React, { Component } from 'react';
import { View, ScrollView, KeyboardAvoidingView, StatusBar, TouchableOpacity, Image, Text, Dimensions, Platform, ToastAndroid } from 'react-native';
import { TextInput } from 'react-native-paper'
import { Constants, Location, Permissions, ImagePicker } from 'expo';
const { width } = Dimensions.get('window');
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import lightMapStyle from '../../style/lightMapStyle';
import deepcopy from 'deepcopy';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import Config from '../../Config.json';

import axios from 'axios';

class AddCheckPoint extends Component {
    static navigationOptions = {
        drawerLabel: () => null,
    };

    state = {
        name: null,
        description: null,
        location: null,
        imageName: null,
        region: {
            latitude: 50.01381,
            longitude: 20.98698,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
        },
        marker: {
            latitude: 50.01381,
            longitude: 20.98698,
        },
    }

    componentWillMount = () => {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            alert('Brak dostępu do lokalizacji');
        }
    }

    componentWillUnmount = () => {
        console.log("unmount")
    }

    componentDidMount = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            alert('Brak dostępu do lokalizacji');
        }

        let location = await Location.getCurrentPositionAsync({});
        this.setState({ location });
    };

    send = () => {
        const { name, description, location, imageName } = this.state;

        if (!name || !description) {
            ToastAndroid.showWithGravity(
                'Proszę uzupełnić dane w formularzu',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        } else if (!location) {
            ToastAndroid.showWithGravity(
                'Proszę wybrać lokalizację',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        } else if (!imageName) {
            ToastAndroid.showWithGravity(
                'Proszę dodać zdjęcie',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        } else {
            const crucialPoints = deepcopy(this.props.crucialPoints);

            crucialPoints.push({
                name, description,
                latitude: location.latitude,
                longitude: location.longitude,
                imagePath: imageName
            });

            this.props.addCrucialPoints(crucialPoints);
            this.props.navigation.navigate("AddRoute");
        }

        console.log(this.props.crucialPoints)
    }

    onChangeText = (value, action) => {
        switch (action) {
            case 'name':
                this.setState({ name: value });
                break;
            case 'description':
                this.setState({ description: value });
                break;
            default:
                break;
        }
        console.log(this.state.name, this.state.description)
    }

    render() {
        let { image } = this.state;

        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                <StatusBar hidden />
                <TextInput
                    label='Nazwa punktu'
                    mode='outlined'
                    theme={{ colors: { placeholder: 'darkgrey', background: 'rgba(255, 255, 255, 1)', text: '#000', primary: '#000' } }}
                    value={this.state.name}
                    secureTextEntry={false}
                    onChangeText={(text) => this.onChangeText(text, 'name')}
                    style={{ marginTop: 10, marginBottom: 10, }}
                    autoCapitalize='none'
                />
                <TextInput
                    label='Opis punktu'
                    mode='outlined'
                    theme={{ colors: { placeholder: 'darkgrey', background: 'rgba(255, 255, 255, 1)', text: '#000', primary: '#000' } }}
                    value={this.state.description}
                    secureTextEntry={false}
                    onChangeText={(text) => this.onChangeText(text, 'description')}
                    style={{ marginTop: 10, marginBottom: 10, }}
                    autoCapitalize='none'
                    multiline={true}
                />
                <View style={{ flex: 1, alignItems: 'center', marginTop: 10, justifyContent: 'flex-start' }}>
                    <TouchableOpacity onPress={() => this._pickImage()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                        <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Dodaj zdjęcie</Text>
                    </TouchableOpacity>
                    {image && <Image source={{ uri: image }} style={{ width: (width - 100), height: (width - 100) }} />}

                    <MapView
                        region={this.state.region}
                        provider={PROVIDER_GOOGLE}
                        style={{ width: (width - 20), height: (width - 20) }}
                        customMapStyle={lightMapStyle}
                        onPress={(coords) => {
                            // console.log(coords.nativeEvent.coordinate)
                            this.setState({ marker: coords.nativeEvent.coordinate, location: coords.nativeEvent.coordinate });
                        }}
                    >
                        <Marker
                            coordinate={this.state.marker}
                            title={'Miejsce wydarzenia'}
                            description={this.state.description}
                        />
                    </MapView>

                    <TouchableOpacity onPress={() => this.send()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                        <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Zatwierdź</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }

    createFormData = (photo) => {
        const data = new FormData();

        data.append("image", {
            name: photo.uri.split('/').pop(),
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        return data;
    };

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false, // higher res on iOS
            aspect: [4, 3],
        });

        if (result.cancelled) {
            return;
        }

        let localUri = result.uri;
        let filename = localUri.split('/').pop();

        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        let formData = new FormData();
        formData.append('image', { uri: localUri, name: filename, type });

        const res = await fetch(`${Config.serverIP}:${Config.serverPORT}/api/image/add`, {
            method: 'POST',
            body: formData,
            header: {
                'content-type': 'multipart/form-data',
            },
        });

        const data = await res.json();

        this.setState({ image: result.uri, imageName: data.fileName });
    }
}

const mapStateToProps = state => {
    return {
        crucialPoints: state.data.crucialPoints
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addCrucialPoints: (crucialPoints) => dispatch(actionCreators.addCrucialPoints(crucialPoints))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCheckPoint);