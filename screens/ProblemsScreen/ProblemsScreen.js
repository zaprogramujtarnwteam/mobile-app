import React, { Component } from 'react';
import { View, Text, StatusBar, ScrollView, Dimensions, TouchableOpacity, Image, Platform, ToastAndroid } from 'react-native';
import MyInput from '../../components/MyInput'
import { Constants, Location, Permissions, ImagePicker } from 'expo';
const { width } = Dimensions.get('window');
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import Header from '../../components/Header/Header';
import Config from '../../Config.json';
import axios from 'axios';

export default class ProblemsScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Zgłoś problem w mieście',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf12a;</Text>
        ),
    };
    state = {
        region: {
            latitude: 50.01381,
            longitude: 20.98698,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
        },
        marker: {
            latitude: 50.01381,
            longitude: 20.98698,
        },
    }

    componentWillMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            alert('Brak dostępu do lokalizacji');
        }
    }

    getLocation = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            alert('Brak dostępu do lokalizacji');
        }

        let location = await Location.getCurrentPositionAsync({});
        let coords = {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
        }
        this.setState({ location: coords, marker: coords, region: coords });
    };

    onChangeText = (value, action) => {
        switch (action) {
            case 'name':
                this.setState({ name: value });
                break;
            case 'description':
                this.setState({ description: value });
                break;
            default:
                break;
        }
    }

    send = async () => {
        const { name, description, location } = this.state;
        if (!name || !description) {
            ToastAndroid.showWithGravity(
                'Proszę uzupełnić dane w formularzu',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
        else if (!location) {
            ToastAndroid.showWithGravity(
                'Proszę wybrać miejsce wydarzenia',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
        else {
            try {
                const res = await axios.post(`${Config.serverIP}:${Config.serverPORT}/api/report/add`, {
                    description,
                    email: name,
                    latitude: location.latitude,
                    longitude: location.longitude,
                    date: new Date(),
                    imagePath: this.state.imageName ? this.state.imageName : null
                });

                console.log(res.data);

                ToastAndroid.showWithGravity(
                    'Twoja zgłoszenie zostało przesłane',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );

                setTimeout(() => this.props.navigation.navigate("Home"), 2000);
            } catch (err) {
                console.log(err);
            }
        }
    }


    render() {
        let { image } = this.state;
        return (
            <View style={{flex: 1}}>
            <Header openDrawer={this.props.navigation.openDrawer.bind()} />
            <ScrollView style={{ margin: 10, marginTop: 35, flex: 1 }}>
                <StatusBar hidden />
                <View>
                <MyInput label={'Adres kontaktowy email'} action={'input'} change={'name'} onChangeText={this.onChangeText} />
                <MyInput label={'Opis problemu'} action={'input'} change={'description'} onChangeText={this.onChangeText} />
                <View style={{ flex: 1, alignItems: 'center', marginTop: 10, justifyContent: 'flex-start' }}>
                    <TouchableOpacity onPress={() => this._pickImage()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                        <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Dodaj zdjęcie</Text>
                    </TouchableOpacity>
                    {image &&
                        <Image source={{ uri: image }} style={{ width: (width - 100), height: (width - 100) }} />}
                    <MapView
                        region={this.state.region}
                        provider={PROVIDER_GOOGLE}
                        style={{ width: (width), height: (width) }}
                        customMapStyle={this.state.mapStyle}
                        onPress={(coords) => {
                            // console.log(coords.nativeEvent.coordinate)
                            this.setState({ marker: coords.nativeEvent.coordinate, location: coords.nativeEvent.coordinate });
                        }}
                    >
                        <Marker
                            coordinate={this.state.marker}
                            title={'Miejsce wydarzenia'}
                            description={this.state.description}
                        />
                    </MapView>
                    <TouchableOpacity onPress={() => this.getLocation()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                        <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Znajdź swoją lokalizację</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.send()} style={{ width: (width - 100), height: 40, borderRadius: 2, backgroundColor: '#285398', alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                        <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Zatwierdź</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </ScrollView>
            </View>
        );
    }
    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false, // higher res on iOS
            aspect: [4, 3],
        });

        if (result.cancelled) {
            return;
        }

        let localUri = result.uri;
        let filename = localUri.split('/').pop();

        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        let formData = new FormData();
        formData.append('image', { uri: localUri, name: filename, type });

        const res = await fetch(`${Config.serverIP}:${Config.serverPORT}/api/image/add`, {
            method: 'POST',
            body: formData,
            header: {
                'content-type': 'multipart/form-data',
            },
        });

        const data = await res.json();

        this.setState({ image: result.uri, imageName: data.fileName });
    }
}

