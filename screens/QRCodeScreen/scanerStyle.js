import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const windowHeight = window.height;
export const windowWidth = window.width;

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        position: "absolute",
        zIndex: -10,
        justifyContent: "center",
        alignItems: "center"
      },
      qr: {
        width: windowWidth*0.7,
        height: windowWidth*0.7,
      },
});
