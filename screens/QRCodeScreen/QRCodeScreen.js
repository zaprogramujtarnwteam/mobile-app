import React, { Component } from 'react';
import { ToastAndroid, Text, View, StatusBar, StyleSheet, Image } from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';
import Config from '../../Config.json';

import axios from 'axios';

import Header from '../../components/Header/Header';
import styles, { windowHeight, windowWidth } from './scanerStyle';

export default class QRCodeScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Skanuj kod QR',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf029;</Text>
        ),
    };
    state = {
        hasCameraPermission: null,
        lastScannedUrl: null,
        pending: false
    };

    componentDidMount() {
        this._requestCameraPermission();
    }

    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };

    _handleBarCodeRead = async result => {
        if (result.data !== this.state.lastScannedUrl && !this.state.pending) {
            try {
                if(/^-{0,1}\d+$/.test(result.data)) return this.props.navigation.navigate("QRDetails", {type: "card", data: result.data })
                if(result.data === "http://www.tarnow.pl/biznes") return this.props.navigation.navigate("QRDetails", {type: "link" })
                this.setState({ pending: true });
                const res = await axios.get(`${Config.serverIP}:${Config.serverPORT}/api/data/place/${result.data}`);
                this.setState({ pending: false });
                this.props.navigation.navigate("QRDetails", { type: "place", data: res.data.place })
            } catch (err) {
                console.log(err);
                ToastAndroid.showWithGravity(
                    'Nieprawidłowy kod QR',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER,
                );
            }
        }
    };

    render() {
        return (
            <View style={{ flex: 1, borderColor: "#285398", borderWidth: 1 }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                {this.state.hasCameraPermission === null
                    ? <Text></Text>
                    : this.state.hasCameraPermission === false
                        ? <Text style={{ color: '#fff' }}>
                        
                        </Text>
                        :
                        <BarCodeScanner
                            onBarCodeRead={this._handleBarCodeRead}
                            style={[StyleSheet.absoluteFill, styles.container]}>
                            <Image
                                style={styles.qr}
                                source={require('../../assets/images/qr.png')}
                            />
                        </BarCodeScanner>
                }
                <StatusBar hidden />
            </View>
        );
    }

}