// modules
import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, TouchableOpacity, Picker, Dimensions, ScrollView, KeyboardAvoidingView } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Location, TaskManager, Permissions } from 'expo';
import { EventRegister } from 'react-native-event-listeners';
import { connect } from 'react-redux';

import Config from '../../Config.json';

// ours components
import MyInput from '../../components/MyInput'

// other
import lightMapStyle from '../../style/lightMapStyle'
import axios from 'axios';
import deepcopy from 'deepcopy';
import * as actionCreators from '../../store/actions/index';
const LOCATION_TASK_NAME = 'background-location-task';

const { width } = Dimensions.get('window');

class AddRouteScreen extends Component {
    static navigationOptions = {
        drawerLabel: () => null
    }
    state = {
        region: {
            latitude: 50.01381,
            longitude: 20.98698,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        marker: {
            latitude: 50.01381,
            longitude: 20.98698,
        },
        locationArray: [],
        btnTitle: 'Lokalizacja Start',
        category: "Spacerowe"
    }

    componentDidMount = async () => {
        const x = await Location.hasStartedLocationUpdatesAsync(LOCATION_TASK_NAME);
        if (!x) {
            this.setState({ btnTitle: 'Lokalizacja Start' });
        }
        else {
            this.setState({ btnTitle: 'Lokalizacja Stop' });
        }
    }

    componentWillMount = async () => {
        this.listener = EventRegister.addEventListener('onChangeLocation', async (data) => {
            let locationData = {
                latitude: data.latitude,
                longitude: data.longitude,
            };
            let array = deepcopy(this.state.locationArray)
            array.push(locationData);
            this.setState({ locationArray: array });
        })
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(this.listener)
    }

    onPress = async () => {
        const x = await Location.hasStartedLocationUpdatesAsync(LOCATION_TASK_NAME);

        if (!x) {
            this.setState({ btnTitle: 'Lokalizacja Stop' });
            await this.on();
        }
        else {
            this.setState({ btnTitle: 'Lokalizacja Start' });
            await this.off();

            const res = await axios.post(`${Config.serverIP}:${Config.serverPORT}/api/route/add`, {
                name: this.state.name,
                description: this.state.description,
                routeType: this.state.category,
                description: this.state.description,
                route: this.state.locationArray
            });

            if (res.status === 200) {
                const crucialPoints = deepcopy(this.props.crucialPoints);
                let i = 0;

                crucialPoints.forEach(async crucialPoint => {
                    const response = await axios.patch(`${Config.serverIP}:${Config.serverPORT}/api/route/add/crucial/${res.data.route._id}`, {
                        name: crucialPoint.name,
                        description: crucialPoint.description,
                        longitude: crucialPoint.longitude,
                        latitude: crucialPoint.latitude,
                        imagePath: crucialPoint.imagePath,
                    });
                    i++;

                    if (i === crucialPoints.length) {
                        const data = await axios.get(`${Config.serverIP}:${Config.serverPORT}/api/route/`);

                        this.props.addRoutes(data.data.routes);
                    }
                    console.log(response.data);
                })
            }
        }
    }

    on = async () => {
        await Permissions.askAsync(Permissions.LOCATION);
        await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
            distanceInterval: 40,
            accuracy: Location.Accuracy.BestForNavigation,
        });
    };

    off = async () => {
        await Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME);
    };

    onChangeText = (key, value) => this.setState({ [key]: value });

    render() {
        return (
            <KeyboardAvoidingView style={{ flex: 1 }}>
                <View style={{ flex: 7 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.onPress()} style={{ height: 40, borderRadius: 2, backgroundColor: '#285398', flex: 1, alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>{this.state.btnTitle}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddCheckPoint')} style={{ height: 40, borderRadius: 2, backgroundColor: '#285398', flex: 1, alignItems: 'center', justifyContent: 'center', margin: 5 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 18, color: 'white' }}>Wyróżnij punkt</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ margin: 20 }}>
                        <MyInput label={'Nazwa trasy'} action={'input'} change={'name'} onChangeText={this.onChangeText.bind(null, "name")} />
                        <MyInput label={'Opis trasy'} action={'input'} change={'name'} onChangeText={this.onChangeText.bind(null, "description")} />
                        <View style={{ borderWidth: 0.5, borderColor: 'rgba(0, 0, 0, 0.5)', borderRadius: 5, padding: 5, marginTop: 5, marginBottom: 5 }}>
                            <Text>Kategoria:</Text>
                            <Picker
                                selectedValue={this.state.category}
                                style={{ height: 35, width: width - 30 }}
                                onValueChange={(itemValue, itemIndex) => {
                                    console.log(itemValue)
                                    this.setState({ category: itemValue })
                                }
                                }>
                                <Picker.Item label="Kulturowe" value="Kulturowe" />
                                <Picker.Item label="Sportowe" value="Sportowe" />
                                <Picker.Item label="Miejskie" value="Miejskie" />
                                <Picker.Item label="Spacerowe" value="Spacerowe" />
                                <Picker.Item label="Historyczne" value="Historyczne" />
                                <Picker.Item label="Turystyczne" value="Turystyczne" />
                                <Picker.Item label="Inne" value="Inne" />
                            </Picker>
                        </View>

                    </View>
                </View>
                <View style={{ flex: 6 }}>
                    <StatusBar hidden />
                    <MapView
                        region={this.state.region}
                        provider={PROVIDER_GOOGLE}
                        style={{ flex: 1, position: 'relative', zIndex: 0, }}
                        customMapStyle={lightMapStyle}
                    >
                        {this.state.locationArray.length > 0 ?
                            this.state.locationArray.map((item) => {
                                console.log(item)
                                return (
                                    <Marker
                                        key={item.latitude}
                                        coordinate={{
                                            latitude: item.latitude,
                                            longitude: item.longitude,
                                        }}
                                    />
                                )
                            })
                            : null}
                    </MapView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}


TaskManager.defineTask(LOCATION_TASK_NAME, async ({ data, error, changeState }) => {
    if (error) {
        //error
    }
    if (data) {
        const { locations } = data;
        EventRegister.emit('onChangeLocation', locations[0].coords);
    }
});

const mapStateToProps = state => {
    return {
        crucialPoints: state.data.crucialPoints
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addRoutes: (routes) => dispatch(actionCreators.addRoutes(routes))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRouteScreen);