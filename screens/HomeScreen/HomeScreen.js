// modules
import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, ImageBackground, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';

// ours components
import Header from '../../components/Header/Header';

// other
import categories from './Categories/categories';
import styles from './categoriesStyle';
import Config from '../../Config';
import * as actionCreators from '../../store/actions/index';

class HomeScreen extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({ tintColor }) => (
      <Text style={{ fontFamily: 'fontAwesome' }}>&#xf015; </Text>
    ),
  };

  componentDidMount = async () => {
    try {
      const res = await axios.get(`${Config.serverIP}:${Config.serverPORT}/api/data/all`);
      this.props.fetchData(res.data.data.BusStop, res.data.data.BikeStation, res.data.data.Route, res.data.data.Weather, res.data.data.Place, res.data.data.Event);
      this.setState({ loading: false });
    } catch (err) {
      //console.log(err)
      console.log("ERROR")
    }
  }

  state = {
    loading: true
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>

        {this.state.loading ? <Spinner
          visible={this.state.loading}
          textContent={'Wczytywanie danych...'}
          textStyle={styles.spinnerTextStyle}
          overlayColor={'rgba(0, 0, 0, 0.4)'}
          animation="fade"
        /> : null}

        <Header openDrawer={this.props.navigation.openDrawer.bind()} />
        <View style={{ margin: 15, marginTop: 35, flex: 1 }}>
          <StatusBar hidden />
          <FlatList
            data={categories}
            keyExtractor={x => x.name}
            renderItem={({ item }) => <TouchableOpacity onPress={this.props.navigation.navigate.bind(null, item.screen)}><ImageBackground source={item.img} imageStyle={{ borderRadius: 5 }} style={[styles.category]}><Text style={styles.categoryName}>{item.name}</Text></ImageBackground></TouchableOpacity>}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    busStops: state.data.BusStop,
    bikeStations: state.data.BikeStation,
    routes: state.data.Route,
    weather: state.data.Weather,
    places: state.data.Place,
    events: state.data.Event,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: (busStops, bikeStations, routes, weather, places, events) => dispatch(actionCreators.fetchData(busStops, bikeStations, routes, weather, places, events))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);