
let categories = [
    {
        name: 'MAPA MIASTA',
        img: require('../../../assets/images/homescreen/citymap.png'),
        screen: "Mapa"
    },
    {
        name: 'TRASY',
        img: require('../../../assets/images/homescreen/routesscreen.png'),
        screen: "Trasy"
    },
    {
        name: 'ZESKANUJ KOD QR',
        img: require('../../../assets/images/homescreen/qrscreen.png'),
        screen: "Skanuj kod QR"
    },
    {
        name: 'WYDARZENIA',
        img: require('../../../assets/images/homescreen/eventsscreen.png'),
        screen: "Wydarzenia"
    },
    {
        name: 'SPRAWDŹ POGODĘ',
        img: require('../../../assets/images/homescreen/wheatherscreen.png'),
        screen: "Sprawdź pogodę"
    },
    {
        name: 'ZGŁOŚ PROBLEM W MIEŚCIE',
        img: require('../../../assets/images/homescreen/problemsscreen.png'),
        screen: "Zgłoś problem w mieście"
    },
    {
        name: 'QUIZ O TARNOWIE',
        img: require('../../../assets/images/homescreen/quizscreen.png'),
        screen: "Rozwiąż quiz"
    },
]

export default categories;