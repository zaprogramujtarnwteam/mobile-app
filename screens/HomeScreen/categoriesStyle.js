import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const windowHeight = window.height;
export const windowWidth = window.width;

export default StyleSheet.create({
    category: {
        margin: 10,
        height: windowHeight / 4,
        resizeMode: "cover",
        borderRadius: 5,
        padding: 20,
        justifyContent: "flex-end",
        alignItems: 'flex-end',
    },
    categoryName: {
        color: "white",
        fontSize: 28,
        textAlign: "right",
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
        fontFamily: 'latoBold',
    },
    spinnerTextStyle: {
        color: '#FFF'
    }
});
