// modules
import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Checkbox } from 'react-native-paper';

// ours components
import Header from '../../components/Header/Header';
import ListItem from '../../components/ListItem'
class EventsScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Wydarzenia',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf6fa;</Text>
        ),
    };
    state = {
        filtred: true
    }

    _renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => {this.props.navigation.navigate("EventDetail", {data: item}); console.log("BENG")} }>
            <ListItem data={item} />
        </TouchableOpacity>
    );

    render() {
        const { filtred } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                <View style={{ margin: 15, marginTop: 35, flex: 1, }}>
                    <View style={{ width: "100%", flexDirection: "row", justifyContent: "center", alignItems: "center"}}>
                        <Text>Filtrowanie nieautoryzowanych</Text>
                        <Checkbox
                            status={filtred ? 'checked' : 'unchecked'}
                            color={"#285398"}
                            onPress={() => { this.setState({ filtred: !filtred }); }}
                        />
                    </View>
                    < StatusBar hidden />
                    <FlatList
                        data={filtred ? this.props.events.filter(x => x.authorized) : this.props.events}
                        keyExtractor={x => x._id}
                        renderItem={this._renderItem}
                    />
                    <View style={{ position: 'absolute', bottom: 20, right: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddEvent')} style={{ width: 205, height: 55, borderRadius: 35, backgroundColor: '#285398', display: 'flex', alignItems: 'center', justifyContent: 'space-around', flexDirection: 'row', borderWidth: 1, borderColor: 'rgba(0, 0, 0, 0)', elevation: 1 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 20, color: 'white' }}>Dodaj wydarzenie</Text><Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: 'white' }}>&#xf067;</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        events: state.data.Event
    };
};

export default connect(mapStateToProps)(EventsScreen);
