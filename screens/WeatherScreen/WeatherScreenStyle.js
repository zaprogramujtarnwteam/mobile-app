import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const windowHeight = window.height;
export const windowWidth = window.width;

export default StyleSheet.create({
    todayWeather: {
        height: windowHeight / 2,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
        width: "100%",
    },

    todayIcon: {
        flex: 2,
        width: "60%",
        padding: 20
    },

    todayText: {
        flex: 3,
        width: "90%",
        marginTop: 10,
    },

    todayTextContent: {
        marginBottom: 6,
        textAlign: "center",
        fontSize: 18,
    },

    weekWeather: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        width: "100%",
        flex: 1,
        marginTop: 24,
        marginBottom: 24,
        borderColor: "#d6d5df",
    },

    weekendDay: {
        margin: 10,
        width: windowWidth/3,
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
    },

    weekTextContent: {
        textAlign: "center",
        fontSize: 13,
    },
});
