import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, Image, } from 'react-native';
import styles, { windowHeight, windowWidth } from './WeatherScreenStyle';

// import redux
import {connect} from 'react-redux';

import Header from '../../components/Header/Header'

class WeatherScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Sprawdź pogodę',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf0c2;</Text>
        ),
    };
    state = {
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()}/>
                <View style={{marginTop: 35, flex: 1, flexDirection: "column", alignItems: "center"}}>
                    <Text style={{width: "100%", textAlign: "center", fontSize: 40, borderBottomWidth: 1, borderColor: "#d6d5df"}}>Tarnów</Text>
                    <View style={styles.todayWeather}>
                        <View style={styles.todayIcon}>
                            <Image style={{flex: 1, width: "100%"}} source={{uri: 'http://openweathermap.org/img/w/'+this.props.weather[0].list[0].weather[0].icon+'.png'}}/>
                        </View>
                        <View style={styles.todayText}>
                            <Text style={styles.todayTextContent}>Temperatura: {(this.props.weather[0].list[0].temp.min-272.15).toFixed(2)}°C - {(this.props.weather[0].list[0].temp.max-272.15).toFixed(2)}°C</Text>
                            <Text style={styles.todayTextContent}>Jakość powietrza: {this.props.weather[0].airPollutionIndex.indexLevelName}</Text>
                            <Text style={styles.todayTextContent}>Wilgotność: {this.props.weather[0].list[0].humidity} %</Text>
                            <Text style={styles.todayTextContent}>Ciśnienie: {this.props.weather[0].list[0].pressure} hPa</Text>
                        </View>
                    </View>
                    <View style={styles.weekWeather}>
                        <FlatList
                            data={this.props.weather[0].list.slice(1)}
                            keyExtractor={x => x.dt.toString()}
                            horizontal={true}
                            renderItem={({item, index}) => 
                                <View style={styles.weekendDay}>
                                    {console.log('http://openweathermap.org/img/w/'+item.weather[0].icon+'.png')}
                                    <Image style={{flex: 1, width: "80%"}} source={{uri: 'http://openweathermap.org/img/w/'+item.weather[0].icon+'.png'}}/>
                                    <Text style={styles.weekTextContent}>Data: {('0' + (new Date(Date.now() + (index+1)*24*60*60*1000 ).getUTCDate())).slice(-2)}.{('0' + (new Date(Date.now() + (index+1)*24*60*60*1000 ).getUTCMonth() + 1)).slice(-2)}</Text>
                                    <Text style={styles.weekTextContent}>{(item.temp.min-272.15).toFixed(2)}°C - {(item.temp.max-272.15).toFixed(2)}°C</Text>
                                </View>
                            }
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
	return {
		busStops: state.data.BusStop,
		bikeStations: state.data.BikeStation,
		routes: state.data.Route,
		weather: state.data.Weather,
		places: state.data.Place
	};
};

export default connect(mapStateToProps)(WeatherScreen);