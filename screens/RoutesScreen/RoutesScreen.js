import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, TouchableOpacity, Dimensions } from 'react-native';

import { connect } from 'react-redux';

import Header from '../../components/Header/Header';
const { width } = Dimensions.get('window')
class RoutesScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Trasy',
        drawerIcon: ({ tintColor }) => (
            <Text style={{ fontFamily: 'fontAwesome' }}>&#xf4d7; </Text>
        ),
    };
    state = {
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header openDrawer={this.props.navigation.openDrawer.bind()} />
                <View style={{ marginTop: 35, flex: 1, flexDirection: "column", alignItems: "center", }}>
                    {/* <Text> {JSON.stringify(this.props.routes)} </Text> */}
                    <FlatList
                        data={this.props.routes}
                        keyExtractor={x => x._id}
                        renderItem={({ item }) =>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', marginTop: 5 }}>
                                <Text style={{ fontFamily: 'fontAwesome', fontSize: 17, width: (width - 200) }}>&#xf4d7; {item.name}</Text>
                                <TouchableOpacity style={{ width: 170, height: 35, borderRadius: 35, backgroundColor: '#285398', display: 'flex', alignItems: 'center', justifyContent: 'space-around', flexDirection: 'row', borderWidth: 1, borderColor: 'rgba(0, 0, 0, 0)', elevation: 1 }} onPress={() => this.props.navigation.navigate('RouteMapScreen', { item: item })}><Text style={{ fontSize: 17, fontFamily: 'latoBold', color: 'white', }}>Pokaż trasę</Text></TouchableOpacity>
                            </View>
                        }
                    />
                    <View style={{ position: 'absolute', bottom: 20, right: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddRoute')} style={{ width: 205, height: 55, borderRadius: 35, backgroundColor: '#285398', display: 'flex', alignItems: 'center', justifyContent: 'space-around', flexDirection: 'row', borderWidth: 1, borderColor: 'rgba(0, 0, 0, 0)', elevation: 1 }}>
                            <Text style={{ fontFamily: 'latoBold', fontSize: 20, color: 'white' }}>Dodaj trasę</Text><Text style={{ fontFamily: 'fontAwesome', fontSize: 20, color: 'white' }}>&#xf067;</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        routes: state.data.Route
    };
};

export default connect(mapStateToProps)(RoutesScreen);