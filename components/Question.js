import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export default class Question extends Component {

    state = {
        selectedAnswer: null
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, margin: 10 }}><Text style={styles.question}>{this.props.data.item.question}</Text></View>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    {this.props.data.item.answers.map((ans, i) => {
                        return <TouchableOpacity style={styles.answer} key={i} onPress={() => { this.setState({ selectedAnswer: i }); this.props.verify(this.props.data.index, ans.correct) }}><View><Text style={this.state.selectedAnswer === i ? { color: '#285398', textAlign: "center" } : { textAlign: "center" }}>{ans.text}</Text></View></TouchableOpacity>
                    })}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    question: {
        textAlign: "center",
        fontSize: 20,
    },
    answer: {
        flex: 1,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'silver',
        padding: 10,
        margin: 10,
        justifyContent: 'center',
        alignItems: "center"
    }
});
