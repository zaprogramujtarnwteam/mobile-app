//modules
import React, { Component } from 'react';
import { View, Text } from 'react-native';

class ListItemText extends Component {
    state = {}

    render() {
        {/* //  uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.data.imagePath}` */ }
        return (
            <View style={{ flex: 1, flexDirection: this.props.direction, justifyContent: 'flex-start', alignItems: 'flex-start', margin: 5, borderBottomWidth: 0.2, borderBottomColor: 'rgba(0, 0, 0, 0.2)', }}>
                <Text style={{ fontFamily: 'latoBold', fontSize: 23, color: 'black', paddingRight: 5 }}>{this.props.title}</Text>
                <Text style={{ fontFamily: 'lato', fontSize: 23, color: 'black' }}>{this.props.value}</Text>
            </View>
        );
    }
}

export default ListItemText;