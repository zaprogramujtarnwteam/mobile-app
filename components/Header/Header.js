import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import headerStyles from './headerStyle';



export default class Header extends Component {
    render() {
        return (
            <View style={headerStyles.header}>
            <TouchableOpacity onPress={this.props.openDrawer}>
              <Text style={{color: "white", fontFamily: 'fontAwesome', fontSize: 30}}>
                &#xf0c9;
              </Text>
            </TouchableOpacity>
            <View style={headerStyles.logoContainer}>
              <Image
                style={{width: 50, height: 55, marginTop: 10}}
                source={require('../../assets/images/logo.png')}
              />
            </View>
            <TouchableOpacity>
              <Text style={{color: "#285398", fontFamily: 'fontAwesome', fontSize: 30}}>
                &#xf0c9;
              </Text>
            </TouchableOpacity>
          </View>
        );
    }
}