import { StyleSheet, Dimensions } from 'react-native';
const window = Dimensions.get('window');

export const windowHeight = window.height;
export const windowWidth = window.width;

export default StyleSheet.create({
    header: {
        width: "100%",
        height: 50,
        backgroundColor: "#285398",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: "center",
        position: 'relative',
        zIndex: 999,
    },

    logoContainer: {
        height: 90,
        width: 90,
        backgroundColor: "#285398",
        borderBottomLeftRadius: 45,
        borderBottomRightRadius: 45,
        zIndex: 10,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
    },
});
