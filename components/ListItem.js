//modules
import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions } from 'react-native';

// ours components
import ListItemText from '../components/ListItemText'
// other
import Config from '../Config';
const { width, height } = Dimensions.get('window');

// this.props.navigation.navigate("EventDetail", {data: obiektZEventem})

class ListItem extends Component {
    state = {}

    render() {
        {/* //  uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.data.imagePath}` */ }
        return (
                <View style={{ flex: 1, flexDirection: 'column', borderWidth: 0.5, borderColor: 'rgba(0, 0, 0, 0.5)',borderRadius: 5, elevation: 1, margin: 5,marginBottom: 15, padding: 10 }}>
                    < ImageBackground source={this.props.data.imagePath ? { uri: `${Config.serverIP}:${Config.serverPORT}/api/image/${this.props.data.imagePath}` } : require('../assets/images/homescreen/eventsscreen.png')} style={{ display: 'flex', flexDirection: 'column', width: (width - 60), height: height / 5, resizeMode: "cover", opacity: 0.7 }
                    } imageStyle={{ borderRadius: 5 }}>
                    </ImageBackground >
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <ListItemText title={'Nazwa wydarzenia:'} value={this.props.data.name} direction={'column'} />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <ListItemText title={'Czas wydarzenia:'} value={new Date(this.props.data.startDate).toLocaleDateString() + " - " + new Date(this.props.data.endDate).toLocaleDateString()} direction={'column'} />
                            </View>
                        </View>
                    </View>
                </View>
        );
    }
}

export default ListItem;