import React, { Component } from 'react';
import { TextInput } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

class MyImput extends Component {
    state = {
        value: null
    }
    render() {
        return (
            this.props.action === 'input' ?
                <TextInput
                    label={this.props.label}
                    mode='outlined'
                    theme={{ colors: { placeholder: 'darkgrey', background: 'rgba(255, 255, 255, 1)', text: '#000', primary: '#000' } }}
                    value={this.state.value}
                    secureTextEntry={false}
                    onChangeText={(text) => {
                        this.props.onChangeText(text, this.props.change)
                        this.setState({ value: text })
                    }}
                    style={{ marginTop: 10, marginBottom: 10, }}
                    autoCapitalize='none'
                />
                : <DatePicker
                    style={{ width: (width - 20), marginTop: 10, marginBottom: 10, }}
                    date={this.state.value}
                    mode="datetime"
                    placeholder={this.props.label}
                    format="YYYY-MM-DD hh:mm a"
                    minDate="2019-04-10"
                    maxDate="2100-06-01"
                    is24Hour={false}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(text) => {
                        this.props.onChangeText(text, this.props.change)
                        this.setState({ value: text })
                    }}
                />
        );
    }
}

export default MyImput;