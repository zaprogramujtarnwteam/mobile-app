import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { createDrawerNavigator, DrawerItems, createAppContainer } from 'react-navigation'
import { Font } from 'expo';
import { View, Text, Dimensions } from 'react-native'
import HomeScreen from "./screens/HomeScreen/HomeScreen";
import MapScreen from "./screens/MapScreen/MapScreen";
const { width } = Dimensions.get('window');
import dataReducer from './store/reducers/data';
import QRCodeScreen from './screens/QRCodeScreen/QRCodeScreen';
import RoutesScreen from './screens/RoutesScreen/RoutesScreen';
import ProblemsScreen from './screens/ProblemsScreen/ProblemsScreen';
import WeatherScreen from './screens/WeatherScreen/WeatherScreen';
import QRCodeDetailScreen from './screens/QRCodeDetailScreen/QRCodeDetailScreen';
import AddRouteScreen from './screens/AddRouteScreen/AddRouteScreen';
import AddCheckPoint from './screens/AddCheckPoint/AddCheckPoint';
import EventsScreen from './screens/EventsScreen/EventsScreen';
import EventDetailScreen from './screens/EventDetailScreen/EventDetailScreen';
import AddEvent from './screens/AddEvent/AddEvent';
import QuizScreen from './screens/QuizScreen/QuizScreen';
import RouteMapScreen from './screens/RouteMapScreen/RouteMapScreen'
const rootReducer = combineReducers({
  data: dataReducer
});

const store = createStore(rootReducer);
const customDrawerComponent = (props) => {
  return (
    <View style={{ flex: 1 }}>
      <DrawerItems {...props} />
    </View>
  )
}
const Navigation = createDrawerNavigator({
  "Home": {
    screen: HomeScreen,
  },
  "Mapa": {
    screen: MapScreen,
  },
  "Trasy": {
    screen: RoutesScreen,
  },
  "Skanuj kod QR": {
    screen: QRCodeScreen,
  },
  "Sprawdź pogodę": {
    screen: WeatherScreen,
  },
  'Wydarzenia': {
    screen: EventsScreen,
  },
  "Zgłoś problem w mieście": {
    screen: ProblemsScreen,
  },
  "QRDetails": {
    screen: QRCodeDetailScreen,
  },
  "AddRoute": {
    screen: AddRouteScreen,
  },
  "EventDetail": {
    screen: EventDetailScreen,
  },
  'AddCheckPoint': {
    screen: AddCheckPoint,
  },
  'AddEvent': {
    screen: AddEvent,
  },
  'Rozwiąż quiz': {
    screen: QuizScreen
  },
  'RouteMapScreen': {
    screen: RouteMapScreen
  },
}, {
    contentComponent: customDrawerComponent,
    drawerWidth: 250,
  });

const AppContainer = createAppContainer(Navigation);
export default class App extends React.Component {
  state = {
    fontloaded: false,
  }
  componentWillMount = async () => {
    await Font.loadAsync({
      'fontAwesome': require('./assets/fonts/fontawesome.ttf'),
      'lato': require('./assets/fonts/Lato-Regular.ttf'),
      'latoBold': require('./assets/fonts/Lato-Bold.ttf'),
    });
    this.setState({ fontloaded: true })
  }

  render() {
    return (
      this.state.fontloaded ?
        <Provider store={store}>
          <View style={{ flex: 1, backgroundColor: "white"}}>
            <AppContainer style={{backgroundColor: "white"}}/>
          </View>
        </Provider> : null
    );
  }
}