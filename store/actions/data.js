import * as actionTypes from './actionTypes';

export const fetchData = (busStops, bikeStations, routes, weather, places, events) => {
	return {
		type: actionTypes.FETCH_DATA,
		busStops,
		bikeStations,
		routes,
		weather,
		places,
		events
	}
}

export const fetchEvents = (events) => {
	return {
		type: actionTypes.FETCH_EVENTS,
		events
	}
}

export const addCrucialPoints = (crucialPoints) => {
	return {
		type: actionTypes.ADD_CRUCIAL_POINT,
		crucialPoints
	}
}

export const addRoutes = (routes) => {
	return {
		type: actionTypes.ADD_ROUTES,
		routes: routes
	}
}