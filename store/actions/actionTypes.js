export const FETCH_DATA = "FETCH_DATA";
export const FETCH_EVENTS = "FETCH_EVENTS";
export const ADD_CRUCIAL_POINT = "ADD_CRUCIAL_POINT";
export const ADD_ROUTES = "ADD_ROUTES";