export {
	fetchData,
	fetchEvents,
	addCrucialPoints,
	addRoutes
} from './data';