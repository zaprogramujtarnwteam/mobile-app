import * as actionTypes from '../actions/actionTypes';

const initialState = {
	BikeStation: null,
	BusStop: null,
	Place: null,
	Weather: null,
	Route: null,
	Event: null,
	crucialPoints: []
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.FETCH_DATA: {
			return {
				...state,
				BusStop: action.busStops,
				BikeStation: action.bikeStations,
				Route: action.routes,
				Weather: action.weather,
				Place: action.places,
				Event: action.events
			}
		}
		case actionTypes.FETCH_EVENTS: {
			return {
				...state,
				Event: action.events
			}
		}
		case actionTypes.ADD_CRUCIAL_POINT: {
			return {
				...state,
				crucialPoints: action.crucialPoints
			}
		}
		case actionTypes.ADD_ROUTES: {
			return {
				...state,
				Route: action.routes
			}
		}
		default:
			return state;
	}
}

export default reducer;